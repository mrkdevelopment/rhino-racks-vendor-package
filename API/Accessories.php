<?php

namespace RhinoRacks\API;

/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright         M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license           http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

/**
 * Class definition for Accessories, based on the WSDL.
 * TODO: Must be implemented if required.
 */
class Accessories extends RhinoRack
{

    public function __construct($options)
    {
        $this->wsdl = 'http://api.rhinorack.com/AccessoryWS.asmx?WSDL';
        parent::__construct($options);
    }

    // This interface returns a list of accessories for a particular accessory sub category in Rhino data store. Each accessory element has information about the accessory like price, part no, description, Fit kit(s) and image gallery etc. Fit kit section of element doesn’t provide the detail information about the fit kit but provide you information about the compatible bar type of roof rack, for the default culture.
    public function GetAccessories($params)
    {
        return $this->request('GetAccessories', array('GetAccessories' => $params));
    }

    // This interface returns a list of accessories for a by a particular datetime string.
    public function GetAccessoriesByDate($params)
    {
        return $this->request('GetAccessoriesByDate', array('GetAccessoriesByDate' => $params));
    }

    // This interface returns a list of accessories for a particular accessory sub category in Rhino data store. Each accessory element has information about the accessory like price, part no, description, Fit kit(s) and image gallery etc. Fit kit section of element doesn’t provide the detail information about the fit kit but provide you information about the compatible bar type of roof rack.
    public function GetAccessoriesByCulture()
    {
    }

    // Returns a list of the accessories for the specified subcategory and system, for the default culture
    public function GetAccessoriesBySubCategory()
    {
    }

    // Returns a list of the accessories for the specified subcategory, system and culture
    public function GetAccessoriesBySubCategoryAndCulture()
    {
    }

    // This interface returns subcategories for a particular main category and the default culture in Rhino store. Return type is xml.
    public function GetCategories($params)
    {
        return $this->request('GetCategories', array('GetCategories' => $params));
    }

    // This interface returns subcategories for a particular main category in Rhino store. Return type is xml.
    public function GetCategoriesByCulture()
    {
    }

    // Returns the categories for the specified main category and system
    public function GetCategoriesForSystem()
    {
    }

    // Returns the categories for the specified main category and system
    public function GetCategoriesForSystemAndCulture()
    {
    }

    // Returns the fit kit for the specified accessory, for the default culture
    public function GetFitkitForAccessory()
    {
    }

    // Returns the fit kit for the specified accessory
    public function GetFitkitForAccessoryAndCulture()
    {
    }

    // This interface provides you a compatible fit kit for a particular accessory and roof rack bar type. This interface provides detail information about the fit kit and image gallery, for the default culture. This interface will return no fit kit if not linked in Rhino data store or fit kit is not required. Schema is same as accessory xml.
    public function GetFitkitForAccessoryAndRoofRack()
    {
    }

    // This interface provides you a compatible fit kit for a particular accessory and roof rack bar type. This interface provides detail information about the fit kit and image gallery. This interface will return no fit kit if not linked in Rhino data store or fit kit is not required. Schema is same as accessory xml.
    public function GetFitkitForAccessoryAndRoofRackByCulture()
    {
    }

    // This interface returns all the main accessory categories in Rhino stores for the default culture.
    public function GetMainCategories()
    {
        return $this->request('GetMainCategories', array('GetMainCategories' => array()));
    }

    // This interface returns all the main accessory categories in Rhino stores.
    public function GetMainCategoriesByCulture()
    {
    }

    // Returns a list of the related items under the specified accessory and sub category, for the default culture
    public function GetRelatedItemsForAccessory()
    {
    }

    // Returns a list of the related items for the specified accessory, sub category and roof rack
    public function GetRelatedItemsForAccessoryAndRoofRack()
    {
    }

    // Returns a list of the related items for the specified accessory, sub category and roof rack
    public function GetRelatedItemsForAccessoryAndRoofRackByCulture()
    {
    }

    // This interface returns a list of related items for a particular accessory and roof rack bar type, for the default culture. This interface will return no related item if not linked in Rhino data store. Schema is same as accessory xml.
    public function GetRelatedItemsForAccessoryAndRoofRackWithoutSubCategories()
    {
    }

    // This interface returns a list of related items for a particular accessory and roof rack bar type. This interface will return no related item if not linked in Rhino data store. Schema is same as accessory xml.
    public function GetRelatedItemsForAccessoryAndRoofRackWithoutSubCategoriesByCulture()
    {
    }

    // Returns a list of the related items under the specified accessory and sub category, by culture
    public function GetRelatedItemsForAccessoryByCulture()
    {
    }

    // Returns a list of the related items for the specified accessory and sub category, for the default culture
    public function GetRelatedItemsForAccessoryWithoutSubCategories()
    {
    }

    // Returns a list of the related items for the specified accessory and sub category
    public function GetRelatedItemsForAccessoryWithoutSubCategoriesByCulture()
    {
    }
    // Returns a list of the sub categories for the specified main category, for the default culture

    public function GetSubCategories()
    {
    }

    // Returns a list of the sub categories for the specified main category and culture
    public function GetSubCategoriesByCulture()
    {
    }
}
