<?php

namespace RhinoRacks\API;

/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright         M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license           http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * API controller for Vehicles
 */
class Vehicles extends RhinoRack
{

     /**
     * Defines the WSDL and contruct the object
     */
    public function __construct($args)
    {
        $this->wsdl = 'http://api.rhinorack.com/VehicleWS.asmx?WSDL';
        parent::__construct($args);

        // $functions = $this->client->__getFunctions ();
        // var_dump ($functions);
    }

    //Returns a list of the body types for the specified model and year, for the default culture
    public function GetVehicleBodyTypes($modelId, $yearId)
    {
        return $this->request('GetVehicleBodyTypes', array('GetVehicleBodyTypes' => array('modelId' => $modelId, 'year' => $yearId)));
    }

    //Returns a list of the body types for the specified model and year, for the specified culture
    public function GetVehicleBodyTypesByCulture()
    {
        return $this->request('GetVehicleBodyTypesByCulture', array());
    }

    //Returns a list of all active vehicle makes for the default culture
    public function GetVehicleMakes()
    {
        return $this->request('GetVehicleMakes', array(
                'GetVehicleMakes' => array(),
            ));
    }

    //Returns a list of all active vehicle makes
    public function GetVehicleMakesByCulture()
    {
        return $this->request('GetVehicleMakesByCulture', array());
    }

    //Returns a list of the years available for the specified model for the default culture
    public function GetVehicleModelYears($modelId)
    {
        return $this->request('GetVehicleModelYears', array('GetVehicleModelYears' => array('modelId' => $modelId) ));
    }

    //Returns a list of the years available for the specified model
    public function GetVehicleModelYearsByCulture()
    {
        return $this->request('GetVehicleModelYearsByCulture', array());
    }

    //Returns a list of all active vehicle models for the specified manufacturer for the default culture
    public function GetVehicleModels($makeId)
    {
        $makeId = $makeId->__toString();

        return $this->request('GetVehicleModels', array('GetVehicleModels' => array('makeId' => $makeId)));
    }

    //Returns a list of all active vehicle models for the specified manufacturer
    public function GetVehicleModelsByCulture()
    {
        return $this->request('GetVehicleModelsByCulture', array());
    }

    //Returns a list of all active vehicles for the specified model, year and body type, for the default culture
    public function GetVehicles($modelId, $yearId, $bodyTypeId)
    {
        return $this->request('GetVehicles', array('GetVehicles' => array('modelId' => $modelId, 'year' => $yearId, 'bodyTypeId' => $bodyTypeId)));
    }

    //Returns a list of all active vehicles for the specified model, year and body type
    public function GetVehiclesByCulture()
    {
        return $this->request('GetVehiclesByCulture', array());
    }

    /**
     * Get Vehicle Information about Rack Roof
     */
    public function GetVehiclesByRoofRack($roofRackId)
    {
        return $this->request('GetVehiclesByRoofRack', array('GetVehiclesByRoofRack' =>array( 'roofRackId' => $roofRackId)));
    }
}
