<?php
namespace RhinoRacks;

/**
 * A transformer class to transform api xml to array.
 */
class BaseProductTransformer
{

    /**
     * Transform function to tranform xml to array
     *
     * @param XML $xml API XML
     *
     * @return array transformed data.
     */
    public function transform($xml)
    {
        $item = array(
                'id'                     => $xml->ID,
                'stock_code'             => $xml->StockCode,
                'name'                   => $xml->Name,
                'description'            => $xml->ShortDescription,
                'feature_description'    => $xml->FeaturesDescription,
                'list_description'       => $xml->ListDescription,
                'last_updated'           => $xml->LastUpdated,
                'price'                  => $xml->Price,
                'feature_image'          => $xml->MainImage,
                'categories'             => $this->getCategories($xml->Categories),
                'media'                  => $this->getMedia($xml->Media),

              );


        if (isset($xml->Specifications) && isset($xml->Specifications->Specification)) {
            if (isset($xml->Specifications->Specification->Name)) {
                $xml->Specifications->Specification = array($xml->Specifications->Specification);
            }

            foreach ($xml->Specifications->Specification as $spec) {
                $item['specifications'][$spec->Name] = $spec->Value;
            }
        }

        $links = $xml->Links;
        $item['links'] = array();

        if (isset($links->Link->Name)) {
            $links->Link = array($links->Link);
        }

        if (is_array($links->Link) && !empty($links->Link)) {
            foreach ($links->Link as $link) {
                $data = array(
                    'name' => $link->Name,
                    'type' => $link->LinkType,
                );

                if ($data['type'] == 'File') {
                    $data['url'] = $link->FileUrl;
                    $item['links'][$link->FileUrl] = $data;
                }
            }
        }

        $item['links'] = array_values($item['links']);


        //PackageComponents

        if (isset($xml->PackageComponents) && isset($xml->PackageComponents->Component)) {
            if (isset($xml->PackageComponents->Component->Name)) {
                $xml->PackageComponents->Component = array($xml->PackageComponents->Component);
            }

            $item['components'] = $xml->PackageComponents->Component;
        }


        if (isset($xml->VehicleLoadRatings) && isset($xml->VehicleLoadRatings->VehicleLoadRating)) {
            if (isset($xml->VehicleLoadRatings->VehicleLoadRating->Name)) {
                $xml->VehicleLoadRatings->VehicleLoadRating = array($xml->VehicleLoadRatings->VehicleLoadRating);
            }

            $item['load_ratings'] = $xml->VehicleLoadRatings->VehicleLoadRating;
        }

        return $item;
    }

    /**
     * Fetch the category information for the category dom object
     *
     * @param XML $categoryDom Category DOM
     *
     * @return array categories
     */
    private function getCategories($categoryDom)
    {
        $categories = array();


        if (isset($categoryDom->Category->MainTitle)) {
            $categoryDom->Category = array($categoryDom->Category);
        }
        foreach ($categoryDom->Category as $category) {
            $categories[] = array('main' => $category->MainTitle, 'sub' =>  $category->Title);
        }

        return $categories;
    }

     /**
     * Fetch the media information for the media dom object
     *
     * @param XML $mediaXml Media DOM
     *
     * @return array media
     */
    private function getMedia($mediaXml)
    {
        $media = array('images' => array(), 'videos' => array());

        if (isset($mediaXml->MediaItem->Title)) {
            $mediaXml->MediaItem = array($mediaXml->MediaItem);
        }

        foreach ($mediaXml->MediaItem as $item) {
            $type = $item->ContentType;

            switch ($type) {
                case 'Image':
                    $media['images'][] = array('url' => $item->Url);
                    break;

                case 'Youtube':
                    $media['videos'][] = array('title'=> $item->Title, 'url' => $item->Url);
                    break;
            }
        }

        return $media;
    }
}
