<?php

namespace RhinoRacks;

use RhinoRacks\API\Accessories;
use RhinoRacks\API\RoofRacks;

class Sync
{

    /**
     * Wrapper function to sync a accessory call.
     *
     * @param array $auth   Crendentials
     * @param array $params Params
     *
     * @return array Response
     */
    public function accessories($auth, $params)
    {
        $api = new Accessories($auth);

        $transformer = new BaseProductTransformer;

        if (!$api->GetAccessoriesByDate($params)) {
            return false;
        }

        $accessories = $api->result();

        $input = json_decode(json_encode($accessories));
        if (!$input->Accessory) {
            return false;
        }

        if (isset($input->Accessory->ID)) {
            $input->Accessory = array($input->Accessory);
        }

        $response = array();

        foreach ($input->Accessory as $accessory) {
            $item = $transformer->transform($accessory);
            $response[] = $item;
        }

        return $response;
    }

    /**
     * Wrapper function to sync a racks api call.
     *
     * @param array $auth   Crendentials
     * @param array $params Params
     *
     * @return array Response
     */
    public function racks($auth, $params)
    {
        $api = new RoofRacks($auth);

        $transformer = new BaseProductTransformer;

        if (!$api->GetRoofRacksForDate($params)) {
            return false;
        }

        $roofRacks = $api->result();
        
        if (!$roofRacks->RoofRack) {
            return false;
        }

        $response = array();

        $input = json_decode(json_encode($roofRacks));
        if (isset($input->RoofRack->ID)) {
            $input->RoofRack = array($input->RoofRack);
        }

        if (isset($input->RoofRack)) {
            foreach ($input->RoofRack as $rack) {
                $item = $transformer->transform($rack);
                $response[] = $item;
            }

            return $response;
        }
    }
}
