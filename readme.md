# Rhino-Rack Web Service SDK

![Rhino Racks Logo](http://www.rhinorack.com/app_themes/rr/images/rr-logo.png)

Rhino Rack PHP Webservice SDK provides API to communicate with the SOAP API endpoint [http://api.rhinorack.com/RoofRackWS.asmx](http://api.rhinorack.com/RoofRackWS.asmx)

This SDK is currently used in the [Magento API Connector Module](https://bitbucket.org/mrkdevelopment/rhino-magento-api-connecter-1.x) to sync racks and accessories into magento.


### Setting up your project via composer.json

This SDK is not available on packagist.org as of now. However, if you would like to use this SDK for your project, please add the following to your `composer.json` file.


1. In the **repositories** section add the follow: 

	`
    {
        "type": "vcs",
        "url":  "git@bitbucket.org:mrkdevelopment/rhino-racks-vendor-package.git"
    }
    `    
2. Add the following **require** entry

	`
	"mrkdevelopment/rhino-racks": "dev-master"
	`
	
3. Run `composer install/update`


API classes for Accessories, Racks and Vehicles can be found in `API` folder.

We use 2 api calls mainly to sync Racks and Accessories

1. `GetAccessoriesByDate` used to sync accessories by date parameter.
2. `GetRoofRacksForDate` used to sync racks by date parameter.

We have a wrapper script for the above two functions. Sample code for both are given below

### Syncing Racks

	
	<?php
	$sync = new \RhinoRacks\Sync;
	
	 $params = ...; //API parameters
	 $auth = .. ; // API auth details.

    $racks  = true;

    while ($racks) {
    	$racks = $sync->racks($auth, $params);
    	
    	// Your application logic code here.
    }   
   



### Syncing Accessories


	<?php
	$sync = new \RhinoRacks\Sync;
	
	 $params = ...; //API parameters
	 $auth = .. ; // API auth details.

    $accessories  = true;

    while ($accessories) {
    	$accessories = $sync->accessories($auth, $params);
    	
    	// Your application logic code here.
    }   
   
   
  

